---
layout: doc
id: OFOCMember
title: Becoming a member of the OBO Operations Committee
---

# Becoming a member of the OBO Foundry Operations Committee (OFOC)

Note: This is _not_ a formal process, but rather a piece of documentation to make the current _informal_ process more transparent. 

The OBO Foundry Operations Committee aims to facilitate the flow of operations within the OBO Foundry.

1. Anyone interested in becoming a member of the Operations Committee should contact an existing group member, who will provide an explanation of
the various duties expected from members (e.g. chairing calls, addressing issues on GitHub, joining a working group).
2. The applicant confirms they are willing to take up some of these duties.
3. Current members are then asked via an email if there are objections to adding the candidate member. 
4. If conflicts/concerns arise, they are discussed at the next available OFOC call to find consensus.
5. If no consensus can be reached, a formal vote is taken among the majority of active members.
6. If consensus is reached to admit the applicant, the applicant will be notified by email. 

Note that new members are strongly encouraged to become members of one of the subcommittees: [Editorial Working Group](https://obofoundry.org/docs/EditorialWG.html)
 or [Technical Working Group](https://obofoundry.org/docs/TechnicalWG.html). 
